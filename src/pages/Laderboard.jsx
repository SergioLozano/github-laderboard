import React, { useState, useEffect, useMemo } from "react";
import { Header, SearchInput } from "../components/datatable";
import Spinner from "../components/spinner";

//Import fetch to access Github API
require("isomorphic-fetch");

const Laderboard = () => {
  const [frameworks, setFrameworks] = useState([]);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState("");
  const ITEMS_PER_PAGE = 10;

  const url =
    "https://api.github.com/search/repositories?q=framework%2Blanguage%3Ajavascript&page=1&per_page=100&sort=stars&order=desc";

  useEffect(() => {
    const abortController = new AbortController();
    const signal = abortController.signal; // signal for abort

    fetch(url, { signal: signal })
      .then((response) => response.json()) // Get response from Github API
      .then((json) => {
        setFrameworks(json.items);
        setLoading(false);
      }); //Store received data on state

    return function cleanup() {
      abortController.abort(); // Cancel all subscriptions by abort
    };
  }, []);

  const headers = [
    { name: "Nombre repositorio", field: "name", sortable: true },
    { name: "URL", field: "url", sortable: false },
  ];

  const laderData = useMemo(() => {
    let computed = frameworks;

    if (search) {
      computed = computed.filter((framework) =>
        framework.name.toLowerCase().includes(search.toLowerCase())
      );
    }

    //Page slicing it shows only 10 items
    return computed.slice(0, ITEMS_PER_PAGE); //Here it should handle full pagination system
  }, [frameworks, search]);

  return (
    <div className="row m-4">
      <div className="col mb-3 col-12">
        <div className="row">
          <div className="col">
            <SearchInput
              onSearch={(value) => {
                setSearch(value);
              }}
            />
          </div>
        </div>
        {loading ? (
          <Spinner />
        ) : (
          <table className="table table-bordered">
            <Header headers={headers} />
            <tbody>
              {laderData.map((framework) => (
                <tr>
                  <td>{framework.name}</td>
                  <td>
                    <a href={framework.url}>{framework.url}</a>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        )}

        {/* TODO: Implement pagination UI */}
      </div>
    </div>
  );
};

export default Laderboard;
