import React from "react";
import { mount } from "enzyme";

import Header from "../header";

describe("table header component", () => {
  it("should renderheaders", () => {
    const headers = [
      { name: "Nombre repositorio", field: "name", sortable: true },
      { name: "URL", field: "url", sortable: false },
    ];

    const wrapper = mount(
      <table>
        <Header headers={headers} />
      </table>
    );
    expect(wrapper.children().prop("headers")).toEqual(headers);
  });
});
