import React from "react";
import ReactDOM from "react-dom";

import { mount } from "enzyme";

import SearchInput from "../search";

describe("search input component", () => {
  it("should renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<SearchInput />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("should change the state after change the input value", () => {
    const changeSearch = jest.fn();
    const wrapper = mount(<SearchInput onSearch={changeSearch} />)
    const handleSearch = jest.spyOn(React, "useState");
    handleSearch.mockImplementation(search => [search, setSearch]);
    
    const searchInput = wrapper.find("input");
    searchInput.simulate("change", { target: { value: "testValue" } });
    expect(changeSearch).toBeTruthy();
  });
});
