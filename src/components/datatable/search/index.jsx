import React, { useState } from "react";

const SearchInput = ({ onSearch }) => {
  const [search, setSearch] = useState("");

  const onInputChange = (value) => {
    setSearch(value);
    onSearch(value);
  };

  return (
    <div className="form-group has-search">
      <span className="fa fa-search search-icon"></span>
      <input
        type="text"
        className="form-control search"
        placeholder="framework"
        value={search}
        onChange={(e) => onInputChange(e.target.value)}
      />
    </div>
  );
};

export default SearchInput;
