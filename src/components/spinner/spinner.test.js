import React from "react";
import { mount } from "enzyme";

import Spinner from "../spinner";

describe("spinner component", () => {
  it("should render default loading text", () => {
    const spinnerComponent = mount(<Spinner />);
    expect(spinnerComponent.find("span").text()).toEqual("Loading...");
  });
});
