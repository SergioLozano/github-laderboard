import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// Global style
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import "./styles/styles.scss";

ReactDOM.render(<App />, document.getElementById('root'));
