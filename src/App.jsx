import React from "react";

//App pages
import Laderboard from "./pages/Laderboard";

const App = () => {
  return <Laderboard />;
};

export default App;
