import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { shallow } from "enzyme";

import Laderboard from "./pages/Laderboard";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("shallow main component without crashing", () => {
  const wrapper = shallow(<App />);
  const laderboard = wrapper.find(Laderboard);
  expect(laderboard.exists()).toBe(true);
});
