# Github Frameworks Laderboard
Laderboard listing frameworks from the [Github API](https://api.github.com/search/repositories?q=framework%2Blanguage%3Ajavascript&page=1&per_page=100&sort=stars&order=desc "Github API").

### Demo
[![Netlify Status](https://api.netlify.com/api/v1/badges/eeec449d-d602-4341-9b59-2281e1a7292f/deploy-status)](https://app.netlify.com/sites/github-laderboard/deploys)

https://github-laderboard.netlify.app/


### Built using

- [ Webpack](https://webpack.js.org/ " Webpack")
- [React](https://reactjs.org/ "React")
- [Bootstrap](https://getbootstrap.com/ "Bootstrap")
- [Sass](https://sass-lang.com/ "Sass")
- [Jest](https://jestjs.io/en/)
- [Enzyme](https://enzymejs.github.io/enzyme/)
- [Netlify](https://www.netlify.com/)

## Running Locally

```bash
$ git clone git@gitlab.com:SergioLozano/github-laderboard.git
$ cd github-laderboard
$ yarn install
$ yarn start
```

## Folder Structure

Please follow the folder structure.

```
github_laderboard
├── coverage
├── README.md
├── node_modules
├── package.json
├── .gitignore
├── public
│   ├── scripts
│   ├── bundle.js
│   └── index.html
└── src
    ├── components
    ├── pages
	├── styles
    ├── App.jsx
    ├── App.test.js
    ├── index.js
    └── setupTests.js
```

#### SRC folder description

🗀 **pages** - Pages are the entry points 

🗀  **components** - Reusable components used in our application

🗀 **styles** - Collection of stylesheets using sass