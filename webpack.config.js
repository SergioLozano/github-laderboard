const webpack = require("webpack");
const path = require("path");

module.exports = {
  entry: "./src/index.js",
  output: {
    path: path.join(__dirname, "public"),
    filename: "bundle.js",
  },

  module: {
    rules: [
      {
        loader: "babel-loader",
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
      },
      {
        /* Include scss extensions and loaders */
        test: /\.(scss|css)$/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },

      {
        /* Include fonts extensions for FontAwesome package */
        test: /\.(svg|eot|woff|woff2|ttf)$/,
        use: ["file-loader"],
      },
    ],
  },
  resolve: {
    extensions: [".js", ".jsx"],
  },
  plugins: [new webpack.HotModuleReplacementPlugin()], // For react hot module replacement (Dev enviroment)
  devServer: {
    contentBase: path.join(__dirname, "public"),
    hot: true,
  },
};
